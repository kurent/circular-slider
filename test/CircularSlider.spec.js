import { expect } from 'chai';
import { } from 'mocha';
import CircularSlider from '../src/js/CircularSlider';

describe('Helper service', () => {


  describe('CircularSlider construction', () => {

    it('should have all defaults set when no values provided', () => {
      let options = {};
      let slider = new CircularSlider(options, true);

      expect(slider.radius).to.be.a('number');
      expect(slider.color).to.be.a('string');
      expect(slider.minValue).to.be.a('number');
      expect(slider.maxValue).to.be.a('number');
      expect(slider.step).to.be.a('number');
      expect(slider.value).to.be.a('number');
      expect(slider.arcWidth).to.be.a('number');
    });

    it('should have all values set from options', () => {

      let options = {
        containerHTML: {},
        onChangeCallback: null,
        onDestroyCallback: null,
        radius: 40,
        color: '#00ff00',
        minValue: -50,
        maxValue: 50,
        step: 10,
        value: -40,
        arcWidth: 20
      };

      let slider = new CircularSlider(options, true);

      expect(slider.radius).to.equal(40);
      expect(slider.color).to.equal('rgba(0,255,0,0.7)');
      expect(slider.minValue).to.equal(-50);
      expect(slider.maxValue).to.equal(50);
      expect(slider.step).to.equal(10);
      expect(slider.value).to.equal(-40);
      expect(slider.arcWidth).to.equal(20);
    });

    describe('CircularSlider methods', () => {

      let options = {
        containerHTML: {},
        onChangeCallback: null,
        onDestroyCallback: null,
        radius: 50,
        color: '#00ff00',
        minValue: 0,
        maxValue: 100,
        step: 1,
        value: 0,
        arcWidth: 20
      };

      let slider = new CircularSlider(options, true);
      slider._setBaseUnit();

      // mock a containerProps.globalCenter function
      slider.containerProps = {
        localCenter: {
          x: 200,
          y: 200
        },
        globalCenter: () => {
          return { 
            x: 400, 
            y: 400
          };
        }
      };

      afterEach(() => {
        slider.value = 0;
        slider.step = 1;
      });

      it('_valueToRadians should transform values into radians', () => {
        expect(slider._valueToRadians(0)).to.equal(0);
        expect(slider._valueToRadians(25)).to.be.closeTo(Math.PI / 2, 0.00001);
        expect(slider._valueToRadians(50)).to.be.closeTo(Math.PI, 0.00001);
        expect(slider._valueToRadians(75)).to.be.closeTo(3 / 2 * Math.PI, 0.00001);
        expect(slider._valueToRadians(100)).to.be.closeTo(2 * Math.PI, 0.00001);
      });

      it('_radiansToValue should transform values into radians', () => {
        expect(slider._radiansToValue(0)).to.equal(0);
        expect(slider._radiansToValue(Math.PI / 2)).to.equal(25);
        expect(slider._radiansToValue(Math.PI)).to.equal(50);
        expect(slider._radiansToValue(3 / 2 * Math.PI)).to.equal(75);
        expect(slider._radiansToValue(2 * Math.PI)).to.equal(100);
      });

      it('_roundToStep should round values to closest step', () => {

        // set Slider's step
        slider.step = 10;

        expect(slider._radiansToValue(0)).to.equal(0);
        expect(slider._radiansToValue(Math.PI / 2)).to.equal(20);
        expect(slider._radiansToValue(Math.PI)).to.equal(50);
        expect(slider._radiansToValue(3 / 2 * Math.PI)).to.equal(80);
        expect(slider._radiansToValue(2 * Math.PI)).to.equal(100);
      });

      it('setValue should set the value of Slider', () => {

        // set value
        expect(slider.value).to.equal(0);
        slider.setValue(57);
        expect(slider.value).to.equal(57);

        // set Slider's step to 10 and set value
        slider.step = 10;
        slider.setValue(57);
        expect(slider.value).to.equal(60);
      });

      it('_getMoveAngle should return an radians value of the angle between point and center', () => {

        expect(slider._getMoveAngle({ x: 400, y: 100 })).to.equal(0);
        expect(slider._getMoveAngle({ x: 600, y: 400 })).to.equal(Math.PI / 2);
        expect(slider._getMoveAngle({ x: 400, y: 600 })).to.equal(Math.PI);
        expect(slider._getMoveAngle({ x: 200, y: 400 })).to.equal(3 / 2 * Math.PI);
        expect(slider._getMoveAngle({ x: 399.99, y: 100 })).to.be.closeTo(2 * Math.PI, 0.01);

      });

      it('_isArcHit should return a boolean when point is on the Arc', () => {

        expect(slider._isArcHit({ x: 200, y: 250 })).to.equal(true);
        expect(slider._isArcHit({ x: 250, y: 200 })).to.equal(true);
        expect(slider._isArcHit({ x: 200, y: 250 })).to.equal(true);
        expect(slider._isArcHit({ x: 150, y: 200 })).to.equal(true);

        expect(slider._isArcHit({ x: 250, y: 250 })).to.equal(false);
        expect(slider._isArcHit({ x: 400, y: 400 })).to.equal(false);
      });

    });
  });


});

import { expect } from 'chai';
import { } from 'mocha';
import HelperService from '../src/js/HelperService';

describe('Helper service', () => {

  it('should have a function degreesToRadians', () => {
    expect(HelperService.degreesToRadians).to.be.a('function');
  });

  it('should have a function radiansToDegrees', () => {
    expect(HelperService.radiansToDegrees).to.be.a('function');
  });

  it('should have a function hexToRgba', () => {
    expect(HelperService.hexToRgba).to.be.a('function');
  });

  describe('Function degreesToRadians', () => {

    it('should properly transform Degrees to Radians', () => {
      expect(HelperService.degreesToRadians(0)).to.equal(0);
      expect(HelperService.degreesToRadians(90)).to.equal(Math.PI / 2);
      expect(HelperService.degreesToRadians(180)).to.equal(Math.PI);
      expect(HelperService.degreesToRadians(270)).to.equal((3 / 2) * Math.PI);
      expect(HelperService.degreesToRadians(360)).to.equal(2 * Math.PI);
    });

  });

  describe('Function radiansToDegrees', () => {

    it('should properly transform Radians to Degrees', () => {
      expect(HelperService.radiansToDegrees(0)).to.equal(0);
      expect(HelperService.radiansToDegrees(Math.PI / 2)).to.equal(90);
      expect(HelperService.radiansToDegrees(Math.PI)).to.equal(180);
      expect(HelperService.radiansToDegrees((3 / 2) * Math.PI)).to.equal(270);
      expect(HelperService.radiansToDegrees(2 * Math.PI)).to.equal(360);
    });

  });

  describe('Function hexToRgba', () => {

    it('should properly transform HEX values to RGBA', () => {
      expect(HelperService.hexToRgba('#ff0000', 0.5)).to.equal('rgba(255,0,0,0.5)');
      expect(HelperService.hexToRgba('#fff', 0.5)).to.equal('rgba(255,255,255,0.5)');
      expect(HelperService.hexToRgba('#00ff00', 0.5)).to.equal('rgba(0,255,0,0.5)');
      expect(HelperService.hexToRgba('#0000ff', 1)).to.equal('rgba(0,0,255,1)');
    });

  });

});

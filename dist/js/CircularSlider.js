(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['exports', './HelperService'], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require('./HelperService'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.HelperService);
    global.CircularSlider = mod.exports;
  }
})(this, function (exports, _HelperService) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  var _HelperService2 = _interopRequireDefault(_HelperService);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var _createClass = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        descriptor.configurable = true;
        if ("value" in descriptor) descriptor.writable = true;
        Object.defineProperty(target, descriptor.key, descriptor);
      }
    }

    return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);
      if (staticProps) defineProperties(Constructor, staticProps);
      return Constructor;
    };
  }();

  var CircularSlider = function () {

    /**
     * Constructor of Circular slider. It accepts one parameter - options object. Options should contain at least containerHTML element
     * others are optional and have default values.
     * 
     * @param {HTMLElement} containerHTML
     * @param {function} onChangeCallback
     * @param {function} onDestroyCallback
     * @param {number} radius
     * @param {string} color
     * @param {number} minValue
     * @param {number} maxValue
     * @param {number} step
     * @param {number} value
     * @param {number} arcWidth
     */
    function CircularSlider(_ref) {
      var containerHTML = _ref.containerHTML,
          onChangeCallback = _ref.onChangeCallback,
          onDestroyCallback = _ref.onDestroyCallback,
          _ref$radius = _ref.radius,
          radius = _ref$radius === undefined ? 50 : _ref$radius,
          _ref$color = _ref.color,
          color = _ref$color === undefined ? '#ff0000' : _ref$color,
          _ref$minValue = _ref.minValue,
          minValue = _ref$minValue === undefined ? 0 : _ref$minValue,
          _ref$maxValue = _ref.maxValue,
          maxValue = _ref$maxValue === undefined ? 100 : _ref$maxValue,
          _ref$step = _ref.step,
          step = _ref$step === undefined ? 1 : _ref$step,
          _ref$value = _ref.value,
          value = _ref$value === undefined ? 0 : _ref$value,
          _ref$arcWidth = _ref.arcWidth,
          arcWidth = _ref$arcWidth === undefined ? 16 : _ref$arcWidth;
      var testMode = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

      _classCallCheck(this, CircularSlider);

      this.containerHTML = containerHTML;
      this.onChangeCallback = onChangeCallback;
      this.onDestroyCallback = onDestroyCallback;
      this.radius = radius;
      this.color = _HelperService2.default.hexToRgba(color, 0.7);
      this.minValue = minValue;
      this.maxValue = maxValue;
      this.step = step;
      this.value = value;
      this.arcWidth = arcWidth;
      this.testMode = testMode;

      // this is and additional parameter for testing purposes since we dont need entire DOM for tests
      if (this.testMode) {
        return this;
      }

      // initialize Slider's internal DOM structure and properties
      if (containerHTML) {
        this._init();
      } else {
        throw Error('No container HTML element provided. This parameter is required.');
      }
    }

    _createClass(CircularSlider, [{
      key: '_init',
      value: function _init() {
        this.HTML = { container: this.containerHTML };
        this.eventListeners = [];
        this.containerProps = this._getContainerProps();
        this._buildSliderDOM();
        this.ctxBackground = this._createCanvasContext();
        this.ctxOverlay = this._createCanvasContext();
        this._addHandleEvents();
        this._setBaseUnit();
        this._setValue(this._valueToRadians(this.value));
        this._drawBackgroundArc();
      }
    }, {
      key: '_setValue',
      value: function _setValue(radians) {

        this.value = this._radiansToValue(radians);

        // skip drawing in test mode
        if (!this.testMode) {

          // draw Arc with already rounded value instead of 'radians', so it snaps to a step.
          this._drawArc(this._valueToRadians(this.value));

          // Rotate handle with radians until de-pressed, so it has a smooth feel
          this._rotateHandle(radians);
        }

        // trigger callback if provided
        this.onChangeCallback && this.onChangeCallback(this.value);
      }
    }, {
      key: 'setValue',
      value: function setValue(value) {
        this._setValue(this._valueToRadians(value));
      }
    }, {
      key: '_rotateHandle',
      value: function _rotateHandle(angle) {
        this.HTML.bar.style.transform = 'rotate(' + (_HelperService2.default.radiansToDegrees(angle) + 180) % 360 + 'deg)';
      }
    }, {
      key: '_valueToRadians',
      value: function _valueToRadians(value) {
        return (value - this.minValue) * this.baseUnit;
      }
    }, {
      key: '_radiansToValue',
      value: function _radiansToValue(radians) {
        return this._roundToStep(radians / this.baseUnit);
      }
    }, {
      key: '_roundToStep',
      value: function _roundToStep(value) {
        return Math.round((value + this.minValue) / this.step) * this.step;
      }
    }, {
      key: '_setBaseUnit',
      value: function _setBaseUnit() {
        this.baseUnit = 2 * Math.PI / (this.maxValue - this.minValue);
      }
    }, {
      key: '_getContainerProps',
      value: function _getContainerProps() {
        var props = this.HTML.container.getBoundingClientRect();

        // center of the Slider inside of its container
        var localCenter = {
          x: props.width / 2,
          y: props.height / 2
        };

        Object.assign(props, { localCenter: localCenter }, { globalCenter: this._getGlobalCenter.bind(this) });

        return props;
      }
    }, {
      key: '_getGlobalCenter',
      value: function _getGlobalCenter() {
        var props = this.HTML.container.getBoundingClientRect();

        return {
          x: props.left + props.width / 2,
          y: props.top + props.height / 2
        };
      }
    }, {
      key: '_createCanvasContext',
      value: function _createCanvasContext() {
        this.HTML.canvas = _HelperService2.default.createDOMElement('canvas-container', 'canvas');

        // set properties and style
        this.HTML.canvas.width = this.containerProps.width;
        this.HTML.canvas.height = this.containerProps.height;
        this.HTML.canvas.style.position = 'absolute';
        this.HTML.wrapper.appendChild(this.HTML.canvas);

        return this.HTML.canvas.getContext('2d');
      }
    }, {
      key: '_buildSliderDOM',
      value: function _buildSliderDOM() {

        var handleSize = this.arcWidth + 5;

        // create wrapper
        this.HTML.wrapper = _HelperService2.default.createDOMElement('c-slider');
        this.HTML.wrapper.style.transform = 'rotate(270deg)'; // todo cleaner

        // create Slider Handle element
        this.HTML.bar = _HelperService2.default.createDOMElement('c-bar');
        this.HTML.handle = _HelperService2.default.createDOMElement('c-handle');

        // style it based on Arc size
        this.HTML.handle.style.height = handleSize + 'px';
        this.HTML.handle.style.width = handleSize + 'px';
        this.HTML.handle.style.left = this.containerProps.width / 2 - this.radius - handleSize / 2 + 'px';
        this.HTML.handle.style.bottom = handleSize / 2 + 'px';

        // put it into DOM
        this.HTML.bar.appendChild(this.HTML.handle);
        this.HTML.wrapper.appendChild(this.HTML.bar);
        this.HTML.container.prepend(this.HTML.wrapper);
      }
    }, {
      key: '_clearCanvas',
      value: function _clearCanvas() {
        this.ctxOverlay.clearRect(0, 0, this.ctxOverlay.canvas.width, this.ctxOverlay.canvas.height);
      }
    }, {
      key: '_drawBackgroundArc',
      value: function _drawBackgroundArc() {
        this.ctxBackground.beginPath();
        this.ctxBackground.arc(this.containerProps.localCenter.x, this.containerProps.localCenter.y, this.radius, 0, 2 * Math.PI);
        this.ctxBackground.lineWidth = this.arcWidth;
        this.ctxBackground.strokeStyle = '#d9d9d9';
        this.ctxBackground.setLineDash([5, 2]);
        this.ctxBackground.stroke();
      }
    }, {
      key: '_drawOverlayArc',
      value: function _drawOverlayArc(toAngle) {
        this.ctxOverlay.beginPath();
        this.ctxOverlay.arc(this.containerProps.localCenter.x, this.containerProps.localCenter.y, this.radius, 0, toAngle);
        this.ctxOverlay.lineWidth = this.arcWidth;
        this.ctxOverlay.strokeStyle = this.color;
        this.ctxOverlay.setLineDash([]);
        this.ctxOverlay.stroke();
      }
    }, {
      key: '_drawArc',
      value: function _drawArc(radianAngle) {
        this._clearCanvas();
        radianAngle && this._drawOverlayArc(radianAngle);
      }
    }, {
      key: '_getMoveAngle',
      value: function _getMoveAngle(point) {
        var center = this.containerProps.globalCenter();

        var radians = Math.atan2(center.y - point.y, center.x - point.x);

        if (radians < 0) {
          radians = radians + 2 * Math.PI; // set to positive values only
        }

        // shift by 270deg so it starts on the top and normalize with modulus
        radians = (radians + 3 / 2 * Math.PI) % (2 * Math.PI);

        return radians;
      }
    }, {
      key: '_addHandleEvents',
      value: function _addHandleEvents() {
        this._registerEventListener(this.HTML.handle, 'touchmove', this._onHandleMove.bind(this));
        this._registerEventListener(this.HTML.handle, 'mousedown', this._onHandlePress.bind(this));
        this._registerEventListener(this.HTML.container, 'mousedown', this._onContainerPress.bind(this));
      }
    }, {
      key: '_registerEventListener',
      value: function _registerEventListener(targetElement, type, handler) {
        targetElement.addEventListener(type, handler);

        // store added eventListeners for cleanup
        var eventListenerRef = {
          targetElement: targetElement,
          type: type,
          handler: handler
        };

        this.eventListeners.push(eventListenerRef);

        return eventListenerRef;
      }
    }, {
      key: '_unregisterEventListener',
      value: function _unregisterEventListener(listener) {
        listener.targetElement.removeEventListener(listener.type, listener.handler);
        return null;
      }
    }, {
      key: '_onHandleMove',
      value: function _onHandleMove(event) {

        // prevent dragging of entire page on mobile
        event.preventDefault();

        // find a current point of move based on event type
        var movePoint = {
          x: event.type === 'touchmove' ? event.targetTouches[0].clientX : event.clientX,
          y: event.type === 'touchmove' ? event.targetTouches[0].clientY : event.clientY
        };

        // To respect Min and Max values on a slider, we must impose some restrictions
        var currentAngle = this._valueToRadians(this.value);
        var newAngle = this._getMoveAngle(movePoint);

        var diff = Math.abs(currentAngle - newAngle);

        /* UX improvement
         *
         * Difference of the newAngle vs current one should not be more than 4/3 of a circle,
         * so user cannot go over the 100% (and 0%).
         * 
         * Because of that, users can have trouble to move slider to max or to min value,
         * since they do it quickly which makes diff over the boundaries. Those two cases
         * we can handle programmatically and help users reach min and max values easier 
         * 
         */
        if (diff < 4 / 3 * Math.PI) {
          this._setValue(newAngle);
        } else if (newAngle < currentAngle) {
          this._setValue(2 * Math.PI);
        } else {
          this._setValue(0);
        }
      }
    }, {
      key: '_onHandlePress',
      value: function _onHandlePress(event) {

        // stop event from propagating to _onContainerPress since we want to move the Handle.
        event.stopImmediatePropagation();

        // register events on body after Handle press so we can track 'mousemove' over entire page
        // save references for cleanup after user drops the Handle
        this.handleMoveListeners = [];
        this.handleMoveListeners.push(this._registerEventListener(document, 'mousemove', this._onHandleMove.bind(this)));
        this.handleMoveListeners.push(this._registerEventListener(document, 'mouseup', this._onHandleDrop.bind(this)));
      }
    }, {
      key: '_onHandleDrop',
      value: function _onHandleDrop(event) {
        var _this = this;

        this._rotateHandle(this._valueToRadians(this.value));
        this.handleMoveListeners.map(function (listener) {
          return _this._unregisterEventListener(listener);
        });
      }
    }, {
      key: '_onContainerPress',
      value: function _onContainerPress(event) {
        this.containerListener = this._registerEventListener(this.HTML.container, 'mouseup', this._onContainerRelease.bind(this));
      }
    }, {
      key: '_onContainerRelease',
      value: function _onContainerRelease(event) {

        // unregister event after Container click
        this._unregisterEventListener(this.containerListener);

        var point = {
          local: {
            x: event.layerX,
            y: event.layerY
          },
          global: {
            x: event.clientX,
            y: event.clientY
          }
        };

        if (this._isArcHit(point.local)) {
          var angle = this._getMoveAngle(point.global);

          this._setValue(angle);
          this._rotateHandle(this._valueToRadians(this.value));
        }
      }
    }, {
      key: '_isArcHit',
      value: function _isArcHit(point) {

        // calculate hypotenuse from Container center to hit point with Pythagorean theorem
        var dx = point.x - this.containerProps.localCenter.x;
        var dy = point.y - this.containerProps.localCenter.y;

        var hypotenuse = Math.pow(dx, 2) + Math.pow(dy, 2);

        // Arc is hit when hypotenuse lies between inner & outer Arc radius
        var innerArc = Math.pow(this.radius - this.arcWidth / 2, 2);
        var outerArc = Math.pow(this.radius + this.arcWidth / 2, 2);

        return hypotenuse >= innerArc && hypotenuse <= outerArc;
      }
    }, {
      key: 'destroy',
      value: function destroy() {
        var _this2 = this;

        // unregister all events when destroying Slider
        this.eventListeners.map(function (listener) {
          return _this2._unregisterEventListener(listener);
        });

        // clean DOM
        this.HTML.wrapper.remove();

        // trigger onDestroy callback
        this.onDestroyCallback && this.onDestroyCallback();
      }
    }]);

    return CircularSlider;
  }();

  exports.default = CircularSlider;
});
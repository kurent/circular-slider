(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['./CircularSlider'], factory);
  } else if (typeof exports !== "undefined") {
    factory(require('./CircularSlider'));
  } else {
    var mod = {
      exports: {}
    };
    factory(global.CircularSlider);
    global.example = mod.exports;
  }
})(this, function (_CircularSlider) {
  'use strict';

  var _CircularSlider2 = _interopRequireDefault(_CircularSlider);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  /* This JS file is just for example and demo purposes. It is not part of Slider code. */

  window.onload = function () {

    // DOM elements
    var sliderContainer = document.getElementById('slider-container');
    var formControls = document.forms.namedItem('form-controls');

    // save all Sliders for removal
    var sliders = [];

    // event to add Slider triggered
    function onAddSlider(event) {

      event.preventDefault();

      var formInputs = event.target.elements;

      // create new Expense element and bind it as 'this' context to Callbacks inside Options so we can update the value and remove it
      var expenseElement = createExpenseElement(formInputs.expenseName.value, formInputs.color.value);

      // prepare Options object for Slider
      var options = {
        containerHTML: sliderContainer,
        onChangeCallback: onSliderChange.bind(expenseElement),
        onDestroyCallback: onSliderDestroy.bind(expenseElement),
        radius: parseInt(formInputs.radius.value),
        color: formInputs.color.value,
        minValue: parseInt(formInputs.minValue.value),
        maxValue: parseInt(formInputs.maxValue.value),
        step: parseFloat(formInputs.step.value),
        value: parseInt(formInputs.minValue.value)

        // create new Slider and add it into array
      };var slider = new _CircularSlider2.default(options);
      sliders.push(slider);

      // add Expense element to DOM
      document.getElementById('expenses').appendChild(expenseElement.wrapper);
    }

    function createExpenseElement(expense, colorHex) {
      var wrapper = document.createElement('div');
      var expenseValue = document.createElement('div');
      var colorBox = document.createElement('div');
      var expenseName = document.createElement('div');

      wrapper.classList = 'expense';
      expenseValue.classList = 'expense-value';
      expenseName.classList = 'expense-name';
      expenseName.innerText = expense;
      colorBox.classList = 'expense-color';
      colorBox.style.background = colorHex;

      wrapper.appendChild(expenseValue);
      wrapper.appendChild(colorBox);
      wrapper.appendChild(expenseName);

      return {
        wrapper: wrapper,
        expenseValue: expenseValue
      };
    }

    // callback function for Slider value change
    function onSliderChange(value) {
      this.expenseValue.innerText = Number.isInteger(value) ? value : value.toFixed(2); // show max 2 decimals
    }

    // callback function for Slider destroyed
    function onSliderDestroy() {
      this.wrapper.remove();
    }

    // event for Slider remove triggered
    function removeSlider() {

      if (sliders.length) {
        var slider = sliders.pop();
        slider.destroy();
      }
    }

    // add event listeners
    document.getElementById('js-remove-slider').addEventListener('click', removeSlider);
    formControls.addEventListener('submit', onAddSlider);
  };
});
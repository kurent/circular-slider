(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['exports'], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports);
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports);
    global.HelperService = mod.exports;
  }
})(this, function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var _createClass = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        descriptor.configurable = true;
        if ("value" in descriptor) descriptor.writable = true;
        Object.defineProperty(target, descriptor.key, descriptor);
      }
    }

    return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);
      if (staticProps) defineProperties(Constructor, staticProps);
      return Constructor;
    };
  }();

  var HelperService = function () {
    function HelperService() {
      _classCallCheck(this, HelperService);
    }

    _createClass(HelperService, null, [{
      key: 'degreesToRadians',
      value: function degreesToRadians(degrees) {
        return degrees * Math.PI / 180;
      }
    }, {
      key: 'radiansToDegrees',
      value: function radiansToDegrees(radians) {
        return radians * 180 / Math.PI;
      }
    }, {
      key: 'hexToRgba',
      value: function hexToRgba(hex, alpha) {
        var c = void 0;
        if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
          c = hex.substring(1).split('');
          if (c.length == 3) {
            c = [c[0], c[0], c[1], c[1], c[2], c[2]];
          }
          c = '0x' + c.join('');
          return 'rgba(' + [c >> 16 & 255, c >> 8 & 255, c & 255].join(',') + (',' + alpha + ')');
        }
        throw new Error('Bad Hex');
      }
    }, {
      key: 'createDOMElement',
      value: function createDOMElement(className) {
        var tag = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'div';

        var element = document.createElement(tag);
        element.classList.add(className);

        return element;
      }
    }]);

    return HelperService;
  }();

  exports.default = HelperService;
});
# Circular slider
This is a pure JS & CSS Slider component, without any external dependencies (not event jQuery).

## Description and goal

Create reusable Circular slider class in javascript (as shown in image below). Make sure to
optimize the code for mobile performance.

* when creating new instance of the slider, pass in the options object
* multiple sliders can be rendered in the same container (see image below)
* each slider should have his own max/min limit and step value
* value number (on the left in the image) should change in real time based on the slider's position
* make sure touch events on one slider don't affect others (even if finger goes out of touched slider range)
* slider value should change when you drag the handle or if you tap the spot on a slider
* the solution should work on mobile devices
* without the use of any external JS libraries
* use GitHub to source your code (make sure you commit early and often)

## Usage

### 1. Get a copy of the plugin ###
You can fork or download this slider.

### 2. Load the required files ###
Inside the page's head tag include the slider's CSS file.

```html
<link rel="stylesheet" href="dist/css/circular-slider.css"/>
```

In the page's footer, just before `</body>`, include the required JavaScript files.

```html
<script src="dist/js/HelperService.js"></script>
<script src="dist/js/CircularSlider.js"></script>
```

### 3. Instantiate the slider ###

```html
<script type="text/javascript">
	window.onload = function () {
		let slider = new CircularSlider({
          containerHTML: HTMLElementContainer,
          onChangeCallback: function(value) { ... },
          onDestroyCallback: function() { ... },
          radius: 50,
          color: '#ff0000',
          minValue: 0,
          maxValue: 100,
          step: 1,
          value: 0,
          arcWidth: 16
        });
	};
</script>
```

### 4. Options and methods ###
Options are pretty self-explanatory. Only the first parameter (containerHTML) is required while other parameters are optional. Please note that color is currently supported only in HEX format.

#### setValue(value) ####
>Sets the value of the slider and creates the overlay

#### destroy() ####
>Call this method to remove the slider from the DOM.


## Demo example
In order to run the example or setup your development environment, you need to install dependencies.

### Prerequisites and dependencies

* node.js (tested on v6.11.0)
* npm (tested on 3.10.10)

### Setup & running the app
Install dependencies and start server

```bash
npm install
npm start
```

## Running in development mode
Running the command below will start a local server with source files watch and Live reload

```bash
npm run dev
```

If you wish run tests with watch functionality, run:

```bash
npm run test:watch
```

## Running tests

Tests for this program can be started from terminal by running a command:

```bash
npm test
```

## Author
Ales Kurent
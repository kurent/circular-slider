import HelperService from './HelperService';

export default class CircularSlider {

  /**
   * Constructor of Circular slider. It accepts one parameter - options object. Options should contain at least containerHTML element
   * others are optional and have default values.
   * 
   * @param {HTMLElement} containerHTML
   * @param {function} onChangeCallback
   * @param {function} onDestroyCallback
   * @param {number} radius
   * @param {string} color
   * @param {number} minValue
   * @param {number} maxValue
   * @param {number} step
   * @param {number} value
   * @param {number} arcWidth
   */
  constructor({
    containerHTML,
    onChangeCallback,
    onDestroyCallback,
    radius = 50,
    color = '#ff0000',
    minValue = 0,
    maxValue = 100,
    step = 1,
    value = 0,
    arcWidth = 16
  }, testMode = false) {

    this.containerHTML = containerHTML;
    this.onChangeCallback = onChangeCallback;
    this.onDestroyCallback = onDestroyCallback;
    this.radius = radius;
    this.color = HelperService.hexToRgba(color, 0.7);
    this.minValue = minValue;
    this.maxValue = maxValue;
    this.step = step;
    this.value = value;
    this.arcWidth = arcWidth;
    this.testMode = testMode;

    // this is and additional parameter for testing purposes since we dont need entire DOM for tests
    if (this.testMode) { 
      return this;
    }

    // initialize Slider's internal DOM structure and properties
    if (containerHTML) {
      this._init();
    } else {
      throw Error('No container HTML element provided. This parameter is required.');
    }
  }

  _init() {
    this.HTML = { container: this.containerHTML };
    this.eventListeners = [];
    this.containerProps = this._getContainerProps();
    this._buildSliderDOM();
    this.ctxBackground = this._createCanvasContext();
    this.ctxOverlay = this._createCanvasContext();
    this._addHandleEvents();
    this._setBaseUnit();
    this._setValue(this._valueToRadians(this.value));
    this._drawBackgroundArc();
  }

  /**
   * This is a private method for setting the value of a Slider
   * @param {number} radians 
   */
  _setValue(radians) {

    this.value = this._radiansToValue(radians);

    // skip drawing in test mode
    if (!this.testMode) {

      // draw Arc with already rounded value instead of 'radians', so it snaps to a step.
      this._drawArc(this._valueToRadians(this.value));

      // Rotate handle with radians until de-pressed, so it has a smooth feel
      this._rotateHandle(radians);
    }

    // trigger callback if provided
    this.onChangeCallback && this.onChangeCallback(this.value);
  }

  /**
   * This is a public method for setting the value of a Slider from outside
   * @param {number} value 
   */
  setValue(value) {
    this._setValue(this._valueToRadians(value));
  }

  _rotateHandle(angle) {
    this.HTML.bar.style.transform = `rotate(${(HelperService.radiansToDegrees(angle) + 180) % 360}deg)`;
  }

  _valueToRadians(value) {
    return (value - this.minValue) * this.baseUnit;
  }

  _radiansToValue(radians) {
    return this._roundToStep(radians / this.baseUnit);
  }

  _roundToStep(value) {
    return Math.round((value + this.minValue) / this.step) * this.step;
  }

  _setBaseUnit() {
    this.baseUnit = (2 * Math.PI / (this.maxValue - this.minValue));
  }

  /* Get all properties of Container element (offsets, size, centers, etc ...) */
  _getContainerProps() {
    const props = this.HTML.container.getBoundingClientRect();

    // center of the Slider inside of its container
    const localCenter = {
      x: props.width / 2,
      y: props.height / 2
    };

    Object.assign(props, { localCenter: localCenter }, { globalCenter: this._getGlobalCenter.bind(this) });

    return props;
  }

  /* Find a global center of Container on demand */
  _getGlobalCenter() {
    const props = this.HTML.container.getBoundingClientRect();

    return {
      x: props.left + props.width / 2,
      y: props.top + props.height / 2
    }
  }

  /* Create HTML5 Canvas element and its Context where we will draw Arcs */
  _createCanvasContext() {
    this.HTML.canvas = HelperService.createDOMElement('canvas-container', 'canvas');

    // set properties and style
    this.HTML.canvas.width = this.containerProps.width;
    this.HTML.canvas.height = this.containerProps.height;
    this.HTML.canvas.style.position = 'absolute';
    this.HTML.wrapper.appendChild(this.HTML.canvas);

    return this.HTML.canvas.getContext('2d');
  }

  /* Creates structure of Slider's DOM and append it into container element */
  _buildSliderDOM() {

    const handleSize = this.arcWidth + 5;

    // create wrapper
    this.HTML.wrapper = HelperService.createDOMElement('c-slider');
    this.HTML.wrapper.style.transform = 'rotate(270deg)'; // todo cleaner

    // create Slider Handle element
    this.HTML.bar = HelperService.createDOMElement('c-bar');
    this.HTML.handle = HelperService.createDOMElement('c-handle');

    // style it based on Arc size
    this.HTML.handle.style.height = `${handleSize}px`;
    this.HTML.handle.style.width = `${handleSize}px`;
    this.HTML.handle.style.left = `${this.containerProps.width / 2 - this.radius - handleSize / 2}px`;
    this.HTML.handle.style.bottom = `${handleSize / 2}px`;

    // put it into DOM
    this.HTML.bar.appendChild(this.HTML.handle);
    this.HTML.wrapper.appendChild(this.HTML.bar);
    this.HTML.container.prepend(this.HTML.wrapper);
  }

  /* Delete entire Canvas */
  _clearCanvas() {
    this.ctxOverlay.clearRect(0, 0, this.ctxOverlay.canvas.width, this.ctxOverlay.canvas.height);
  }

  /* Draw background Arc with dashed style */
  _drawBackgroundArc() {
    this.ctxBackground.beginPath();
    this.ctxBackground.arc(this.containerProps.localCenter.x, this.containerProps.localCenter.y, this.radius, 0, 2 * Math.PI);
    this.ctxBackground.lineWidth = this.arcWidth;
    this.ctxBackground.strokeStyle = '#d9d9d9';
    this.ctxBackground.setLineDash([5, 2]);
    this.ctxBackground.stroke();
  }

  /* Draw Arc with color - Overlay */
  _drawOverlayArc(toAngle) {
    this.ctxOverlay.beginPath();
    this.ctxOverlay.arc(this.containerProps.localCenter.x, this.containerProps.localCenter.y, this.radius, 0, toAngle);
    this.ctxOverlay.lineWidth = this.arcWidth;
    this.ctxOverlay.strokeStyle = this.color;
    this.ctxOverlay.setLineDash([]);
    this.ctxOverlay.stroke();
  }

  /**
   * Draw and Arc to an angle provided in a parameter
   * @param {number} radianAngle 
   */
  _drawArc(radianAngle) {
    this._clearCanvas();
    radianAngle && this._drawOverlayArc(radianAngle);
  }

  /**
   * Calculate the angle between center of Slider and a point
   * @param {Object} point 
   */
  _getMoveAngle(point) {
    const center = this.containerProps.globalCenter();

    let radians = Math.atan2(center.y - point.y, center.x - point.x);

    if (radians < 0) {
      radians = radians + (2 * Math.PI); // set to positive values only
    }

    // shift by 270deg so it starts on the top and normalize with modulus
    radians = (radians + 3 / 2 * Math.PI) % (2 * Math.PI);

    return radians;
  }

  /* Register initial events to listen for Handle press, drag and arc click */
  _addHandleEvents() {
    this._registerEventListener(this.HTML.handle, 'touchmove', this._onHandleMove.bind(this));
    this._registerEventListener(this.HTML.handle, 'mousedown', this._onHandlePress.bind(this));
    this._registerEventListener(this.HTML.container, 'mousedown', this._onContainerPress.bind(this));
  }

  /**
   *  Helper method to store all event listeners for later cleanup
   * @param {HTMLElement} targetElement
   * @param {string} type
   * @param {function} handler
   */
  _registerEventListener(targetElement, type, handler) {
    targetElement.addEventListener(type, handler);

    // store added eventListeners for cleanup
    const eventListenerRef = {
      targetElement: targetElement,
      type: type,
      handler: handler
    };

    this.eventListeners.push(eventListenerRef);

    return eventListenerRef;
  }

  /**
   * Helper function to remove Event listeners from an element
   * @param {Object} listener 
   */
  _unregisterEventListener(listener) {
    listener.targetElement.removeEventListener(listener.type, listener.handler);
    return null;
  }

  _onHandleMove(event) {

    // prevent dragging of entire page on mobile
    event.preventDefault();

    // find a current point of move based on event type
    const movePoint = {
      x: event.type === 'touchmove' ? event.targetTouches[0].clientX : event.clientX,
      y: event.type === 'touchmove' ? event.targetTouches[0].clientY : event.clientY
    };

    // To respect Min and Max values on a slider, we must impose some restrictions
    const currentAngle = this._valueToRadians(this.value);
    const newAngle = this._getMoveAngle(movePoint);

    const diff = Math.abs(currentAngle - newAngle);

    /* UX improvement
     *
     * Difference of the newAngle vs current one should not be more than 4/3 of a circle,
     * so user cannot go over the 100% (and 0%).
     * 
     * Because of that, users can have trouble to move slider to max or to min value,
     * since they do it quickly which makes diff over the boundaries. Those two cases
     * we can handle programmatically and help users reach min and max values easier 
     * 
     */
    if (diff < (4 / 3) * Math.PI) {
      this._setValue(newAngle);

    } else if (newAngle < currentAngle) {
      this._setValue(2 * Math.PI);

    } else {
      this._setValue(0);
    }
  }

  /**
   * Handler triggered when user presses on a Slider's handle. It registers new listeners to handle Move & mouse depress.
   * @param {MouseEvent} event 
   */
  _onHandlePress(event) {

    // stop event from propagating to _onContainerPress since we want to move the Handle.
    event.stopImmediatePropagation();

    // register events on body after Handle press so we can track 'mousemove' over entire page
    // save references for cleanup after user drops the Handle
    this.handleMoveListeners = [];
    this.handleMoveListeners.push(this._registerEventListener(document, 'mousemove', this._onHandleMove.bind(this)));
    this.handleMoveListeners.push(this._registerEventListener(document, 'mouseup', this._onHandleDrop.bind(this)));
  }

  /**
   * Handler triggered when user de-presses Slider's handle. It un-registers previously set listeners and rotate Handle accordingly.
   * @param {MouseEvent} event 
   */
  _onHandleDrop(event) {
    this._rotateHandle(this._valueToRadians(this.value));
    this.handleMoveListeners.map((listener) => this._unregisterEventListener(listener));
  }

  /**
   * Handle triggered when user clicks somewhere in the Container (except on a Slider's handle). It registers the event to
   * listen for a button|touche release.
   * @param {MouseEvent} event
   */
  _onContainerPress(event) {
    this.containerListener = this._registerEventListener(this.HTML.container, 'mouseup', this._onContainerRelease.bind(this));
  }

  /**
   * Handler triggered when user releases a click made on Container. We need to check if the click was on the Arc, so we can
   * set the value accordingly.
   * @param {MouseEvent} event
   */
  _onContainerRelease(event) {

    // unregister event after Container click
    this._unregisterEventListener(this.containerListener);

    const point = {
      local: {
        x: event.layerX,
        y: event.layerY
      },
      global: {
        x: event.clientX,
        y: event.clientY
      }
    };

    if (this._isArcHit(point.local)) {
      const angle = this._getMoveAngle(point.global);

      this._setValue(angle);
      this._rotateHandle(this._valueToRadians(this.value));
    }
  }

  /**
   * Check if point is on the Arc
   * @param {Object} point - {x: xVal, y: yVal}
   * @returns {boolean}
   */
  _isArcHit(point) {

    // calculate hypotenuse from Container center to hit point with Pythagorean theorem
    const dx = point.x - this.containerProps.localCenter.x;
    const dy = point.y - this.containerProps.localCenter.y;

    const hypotenuse = Math.pow(dx, 2) + Math.pow(dy, 2);

    // Arc is hit when hypotenuse lies between inner & outer Arc radius
    const innerArc = Math.pow(this.radius - this.arcWidth / 2, 2);
    const outerArc = Math.pow(this.radius + this.arcWidth / 2, 2);

    return hypotenuse >= innerArc && hypotenuse <= outerArc;
  }

  /**
   * Removes all Slider's DOM elements and unregister listeners. It also calls onDestroyCallback if provided.
   */
  destroy() {

    // unregister all events when destroying Slider
    this.eventListeners.map((listener) => this._unregisterEventListener(listener));

    // clean DOM
    this.HTML.wrapper.remove();

    // trigger onDestroy callback
    this.onDestroyCallback && this.onDestroyCallback();
  }
}
import CircularSlider from './CircularSlider';

/* This JS file is just for example and demo purposes. It is not part of Slider code. */

window.onload = function () {

  // DOM elements
  const sliderContainer = document.getElementById('slider-container');
  const formControls = document.forms.namedItem('form-controls');

  // save all Sliders for removal
  let sliders = [];

  // event to add Slider triggered
  function onAddSlider(event) {

    event.preventDefault();

    const formInputs = event.target.elements;

    // create new Expense element and bind it as 'this' context to Callbacks inside Options so we can update the value and remove it
    const expenseElement = createExpenseElement(formInputs.expenseName.value, formInputs.color.value);

    // prepare Options object for Slider
    const options = {
      containerHTML: sliderContainer,
      onChangeCallback: onSliderChange.bind(expenseElement),
      onDestroyCallback: onSliderDestroy.bind(expenseElement),
      radius: parseInt(formInputs.radius.value),
      color: formInputs.color.value,
      minValue: parseInt(formInputs.minValue.value),
      maxValue: parseInt(formInputs.maxValue.value),
      step: parseFloat(formInputs.step.value),
      value: parseInt(formInputs.minValue.value)
    }

    // create new Slider and add it into array
    const slider = new CircularSlider(options);
    sliders.push(slider);
    
    // add Expense element to DOM
    document.getElementById('expenses').appendChild(expenseElement.wrapper);
  }

  function createExpenseElement(expense, colorHex) {
    let wrapper = document.createElement('div');
    let expenseValue = document.createElement('div');
    let colorBox = document.createElement('div');
    let expenseName = document.createElement('div');

    wrapper.classList = 'expense';
    expenseValue.classList = 'expense-value';
    expenseName.classList = 'expense-name';
    expenseName.innerText = expense;
    colorBox.classList = 'expense-color';
    colorBox.style.background = colorHex;

    wrapper.appendChild(expenseValue);
    wrapper.appendChild(colorBox);
    wrapper.appendChild(expenseName);

    return {
      wrapper: wrapper,
      expenseValue: expenseValue
    };
  }

  // callback function for Slider value change
  function onSliderChange(value) {
    this.expenseValue.innerText = Number.isInteger(value) ? value : value.toFixed(2); // show max 2 decimals
  }

  // callback function for Slider destroyed
  function onSliderDestroy() {
    this.wrapper.remove();
  }

  // event for Slider remove triggered
  function removeSlider() {

    if (sliders.length) {
      let slider = sliders.pop();
      slider.destroy();
    }
  }

  // add event listeners
  document.getElementById('js-remove-slider').addEventListener('click', removeSlider);
  formControls.addEventListener('submit', onAddSlider);
}
export default class HelperService {

  static degreesToRadians(degrees) {
    return degrees * Math.PI / 180;
  }

  static radiansToDegrees(radians) {
    return radians * 180 / Math.PI;
  }

  /**
   * Transform HEX color value into RGBA values
   * @param {string} hex 
   * @param {number} alpha 
   */
  static hexToRgba(hex, alpha) {
    let c;
    if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
      c = hex.substring(1).split('');
      if (c.length == 3) {
        c = [c[0], c[0], c[1], c[1], c[2], c[2]];
      }
      c = '0x' + c.join('');
      return 'rgba(' + [(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',') + `,${alpha})`;
    }
    throw new Error('Bad Hex');
  }

  /**
   * Helper function for creation of DOM elements
   * @param {string} className 
   * @param {string} tag 
   */
  static createDOMElement(className, tag = 'div') {
    let element = document.createElement(tag);
    element.classList.add(className);

    return element;
  }
}